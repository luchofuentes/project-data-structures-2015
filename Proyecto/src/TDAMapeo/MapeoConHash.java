package TDAMapeo;

import TDALista.*;

import java.util.Iterator;


public class MapeoConHash <K,V>implements Map<K, V> {

	protected DoubleLinkedList<Entrada<K,V>>[] S;
	protected int n;   //cantidad de entradas de mapeos en el arreglo
	protected int N;   //Tiene que ser primo, tamaño del arreglo

	public MapeoConHash (){
		this(13);
	}
	
	/**
	 * Crea un objeto MapeoConHash con una capacidad que le pasan por parametro
	 * @param cap Capacidad del mapeo.
	 */
	public MapeoConHash (int cap){
		n=0;
		N=cap;
		S= (DoubleLinkedList <Entrada<K,V>>[]) new DoubleLinkedList[N];
		
		for (int i=0; i<N ; i++){
			S[i]= new DoubleLinkedList <Entrada<K,V>>();
		}
	}
	
	public int size() {
		
		return n;
	}

	public boolean isEmpty() {
		
		return (n==0);	
	}

	public V get(K key) throws InvalidKeyException{
		
		if (key==null)
			throw new InvalidKeyException ("clave invalida");
		
		int posicion= encontrarPosicion(key);
			
		Iterator<Position<Entrada<K,V>>> it = S[posicion].positions().iterator();
		Position<Entrada<K,V>> posIt;
			
		while(it.hasNext())
		{
			posIt = it.next();
			if(posIt.element().getKey().equals(key))
				return posIt.element().getValue();
							
		}
		
		return null;
	}	
			
	public V put(K key, V value) throws InvalidKeyException {
		
		
		if(key==null)
			throw new InvalidKeyException ("put: clave nula");
		
		int pos;
		

		if ((n/N) < 1)
			pos= (encontrarPosicion(key));
		else
		{
			resize(N);
			pos= (encontrarPosicion(key));
		}
		
		
		
		if (S[pos].isEmpty())
		{
			S[pos].addFirst(new Entrada<K,V>(key,value));
			n++;
		}
		
		else
		{
			try 
			{ 
				boolean pertenece= false;
				
				Iterator<Position<Entrada<K, V>>> it= S[pos].positions().iterator();
				
				Position<Entrada<K, V>> posIt=null;
				
				while (it.hasNext() && !pertenece){
					
					 posIt= it.next();
					 pertenece=posIt.element().getKey().equals(key);
						
				}
			
				if(!pertenece)
				{
					S[pos].addLast(new Entrada <K,V> (key, value));
					n++;
				}
				else
					S[pos].set(posIt, new Entrada <K,V>(key,value));
				
				
				return S[pos].last().element().getValue();
			}
			
			catch (EmptyListException e)
			{
				e.printStackTrace();
			}	
			catch (InvalidPositionException e)
			{
				e.printStackTrace();
			}
		}
			
		return null;
			
	}
	
	public V remove(K key) throws InvalidKeyException {
		
		if (key==null)
			throw new InvalidKeyException ("Clave Invalida");
		
		int pos= encontrarPosicion(key);
		boolean encontre = false;
		V ret = null;
		
		Iterator<Position<Entrada<K,V>>> it = S[pos].positions().iterator();
		Position<Entrada<K,V>> posicion = null;
		
		while(it.hasNext() && !encontre)
		{
			posicion = it.next();
			if(posicion.element().getKey().equals(key))
			{
				encontre=true;
				try 
				{
					ret = S[pos].remove(posicion).getValue();
					n--;
				} catch (InvalidPositionException e) {
					e.printStackTrace();
				}
			}
		}
			
		return ret;
	}
 
	public Iterable<K> keys() {
		
		PositionList<K> salida= new DoubleLinkedList <K> ();	
			
		for (int i=0; i<N; i++){
					
			for (Position<Entrada<K, V>> e:  S[i].positions()){
						
				salida.addLast(e.element().getKey());
						
			}
		}
				
		return  salida; 
	} 
		
	public Iterable<V> values(){ 
	
		PositionList<V> salida= new DoubleLinkedList <V> ();
	
		for (int i=0; i<N ; i++){
				
			for (Position<Entrada<K, V>> e:  S[i].positions()){
				
				salida.addLast(e.element().getValue());
				
			}
		}
		
		return  salida; 
	}

	public Iterable<Entry<K, V>> entries() {
	
		PositionList <Entry<K,V>> salida= new DoubleLinkedList <Entry<K,V>> ();
	
		for (int i=0; i<N; i++){
				
			for (Position<Entrada<K, V>> e:  S[i].positions()){
			
				salida.addLast(e.element());
			}
		}
	
		return  salida; 
	} 
	
	/*##### Metodos Privados #####*/

	private int encontrarPosicion (K key){
		
		return ((int)(Math.abs(key.hashCode()) % N));
		
	}
	
	private int siguientePrimo(int n){
	   
		boolean esPrimo = false;
	    int m = (int) Math.ceil(Math.sqrt(n));
	    
	    if (n % 2 == 0){
	        n = n + 1;
	    }
	    while (!esPrimo){
	       
	    	esPrimo = true;
	        
	    	for (int i = 3; i <= m; i+=2) 
	            if (n % i == 0) {
	                esPrimo = false;
	                break;
	            }
	    	
	        if (!esPrimo) {
	            n = n + 2;
	        }
	    }
	    return n;
	}
	
	private void resize(int cap) { 
		 
		PositionList<Entry<K,V>> ListaAux= new DoubleLinkedList<Entry<K,V>> ();
		
		for (Entry<K,V> e: entries())
			ListaAux.addLast(e);
									
		
		N=siguientePrimo(cap*2);
		
		
		DoubleLinkedList<Entrada<K,V>>[] Saux;
		Saux= (DoubleLinkedList <Entrada<K,V>>[]) new DoubleLinkedList[N];
		
		
		for (int i=0; i<N ; i++){
			Saux[i]= new DoubleLinkedList <Entrada<K,V>>();
		}
		
		S=Saux;

		for (Position<Entry<K,V>> ee: ListaAux.positions())
			try{
				put(ee.element().getKey(),ee.element().getValue());
				n--;
			} 
			catch (InvalidKeyException e1) {
				e1.printStackTrace();
			}
				
	}
			
	
}
