package TDAMapeo;

public class InvalidKeyException extends Exception {

		/**
		 * TDAMapeo: excepcion que se lanza si no es una clave valida
		 * @param msg Mensaje de error
		 */
		public InvalidKeyException (String msg){
			super (msg);
		}
}
