package TDAMapeo;

public class Entrada <K,V> implements Entry<K,V>{
	private K clave;
	private V valor;
	
	public Entrada (K k, V v){
		clave=k;
		valor=v;
		
	}
	
	public K getKey (){
		return clave;
	}
	
	public V getValue(){
		return valor;
	}
	
	/**
	 * Setea la clave a la entrada
	 * @param k Clave a setear a la entrada
	 */
	public void setKey(K k){
		clave=k;
	}
	
	/**
	 * Setea el valor a la entrada
	 * @param v Valor a setear a la entrada
	 */
	public void setValue (V v){
		valor= v;
	}
}
