package TDAMapeo;

public interface Entry<K,V> {  
	
	/**
	 * Devuelve la clave de la entrada. 
	 * @return Clave K de la entrada .
	 * 
	 */
	public K getKey();  
	
	/**
	 * Devuelve el valor de la entrada. 
	 * @return Valor V de la entrada .
	 * 
	 */
	public V getValue ();  
}