package TDAGrafo;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public interface Graph<V, E> { 
	 
	 /**
	 * Retorna una coleccion iterable con todos los vertices del grafo
	 * @return Una coleccion iterable con todos los vertices del grafo 
	 */
	 public Iterable<Vertex<V>> vertices(); 
	 
	 /**
	  * Retorna una colección iterable con todos los vértices del grafo.
	  * @return Una colección iterable con todos los vertices del grafo.
	  */	 
	 public Iterable<Edge<E>> edges();
	 
	 /** 
	  * Retorna una colección iterable con todos los arcos incidentes sobre un vértice v
	  * @param v vertice a buscar sus arcos incidentes
	  * @return Una colección iterables con todos los arcos incidentes sobre un vértice v  
	  * @throws InvalidVertexException si el vértice no es valido.
	  */
	 public Iterable<Edge<E>> incidentEdges(Vertex<V> v) throws InvalidVertexException;
	 
	 /**
	  * Retorna una colección iterable con todos los arcos emergentes sobre un vértice v
	  * @param v vertice a buscar sus arcos emergentes
	  * @return Una colección iterables con todos los arcos emergentes sobre un vértice v
	  * @throws InvalidVertexException si el vértice no es valido.
	  */
	 public Iterable<Edge<E>> emergentEdges(Vertex<V> v) throws InvalidVertexException;
	 
	 /**
	  * Retorna el otro vértice w del arco e=(v,w); ocurre un error
	  * si e no es incidente(o emergente de v).
	  * @param v Vértice.
	  * @param e Arco.
	  * @return El otro vértice w del arco e=(v,w).
	  * @throws InvalidVertexException si el vértice no es valido.
	  * @throws InvalidEdgeException si el arco no es valido.
	  */
	 public Vertex<V> opposite(Vertex<V> v, Edge<E> e) throws InvalidVertexException, InvalidEdgeException;
	 
	 /**
	  * Retorna un arreglo (de 2 componentes) conteniendo los vértices del arco e. 
	  * @param e Arco a buscar sus vértices
	  * @return Un arreglo (de 2 componentes) conteniendo los vértices del arco e. 
	  * @throws InvalidEdgeException si el arco no es válido
	  */
	 public Vertex<V> [] endVertices(Edge<E> e) throws InvalidEdgeException;
	 
	 /**
	  * Testea si los vértices v y w son adyacentes.
	  * @param v Vértice 
	  * @param w Vértice
	  * @return Verdadero si los vértices v y w son adyacentes, caso contrario retorna Falso.
	  * @throws InvalidVertexException si algunos de los vértices no es valido.
	  */
	 public boolean areAdjacent(Vertex<V> v, Vertex<V> w) throws InvalidVertexException;
	 
	 /**
	  * Reemplaza el rótulo del vértice v conx
	  * @param v Vértice a cambiar el rótulo
	  * @param x Elemento nuevo para el vertice v
	  * @return Elemento del vertice reemplazado
	  * @throws InvalidVertexException si el vértice no es valido.
	  */
	 public V replace( Vertex<V> v, V x) throws InvalidVertexException;
	 
	 /**
	  * Reemplaza el rótulo del arco e con x.
	  * @param e Arco a reemplazar.
	  * @param x Rótulo a insertar.
	  * @return Rótulo reemplazado.
	  * @throws InvalidEdgeException si el arco no es valido.
	  */
	 public E replace( Edge<E> e, E x) throws InvalidEdgeException;
	 
	 /**
	  * Inserta un vertice con rotulo x y retorna el vertice creado
	  * @param x Elemento del nuevo vértice
	  * @return Un nuevo vértice con rótulo x
	  */
	 public Vertex<V> insertVertex(V x);
	 
	 
	 /**
	  * Inserta y retorna un nuevo arco dirigido con origen v y destino w , y rotulo x.
	  * @param v Vertice de origen.
	  * @param w Vertice de destino.
	  * @param x Rótulo del nuevo arco.
	  * @return Un nuevo arco dirigido.
	  * @throws InvalidVertexException si algunos de los vértice no es valido.
	  */
	 public Edge<E> insertDirectedEdge(Vertex<V> v,Vertex<V> w, E x) throws InvalidVertexException;
	 
	 /**
	  * Elimina el vértice v y todos sus arcos adyacentes y retorna el rótulo de v 
	  * @param v vertice a eliminar 
	  * @return El rótulo de v
	  * @throws InvalidVertexException si el vértice no es valido.
	  */
	 public V removeVertex(Vertex<V> v) throws InvalidVertexException;
	 
	 /**
	  * Elimina el arco e y retorna el rótulo almacenado en e.
	  * @param e Arco a eliminar.
	  * @return El rótulo del arco eliminado.
	  * @throws InvalidEdgeException si el arco no es valido.
	  */
	 public E removeEdge(Edge<E> e) throws InvalidEdgeException; 
} 