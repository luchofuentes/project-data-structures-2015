package TDAGrafo;

import TDALista.Position;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class Arco <V,E> implements Edge <E>{
	
	private E rotulo;
	private Vertice <V,E> sucesor, predecesor;
	private Position <Arco<V,E>> posicionEnAdyacentes;
	
	public Arco (E e,Vertice <V,E> predecesor, Vertice <V,E> sucesor){
		
		this.rotulo=e;
		this.sucesor=sucesor;
		this.predecesor=predecesor;
		
	}
	public Arco (Vertice <V,E> predecesor, Vertice <V,E> sucesor){
		
		this(null,predecesor,sucesor);
		
	}
	
	public E element()
	{
		return rotulo; //Comentario
	} 
	
	
	public Vertice <V,E> getPredecesor (){
		return predecesor;
	}
	
	public Vertice <V,E> getSucesor (){
		return sucesor;
	}
	
	public Position<Arco<V,E>> getPosicionEnAdyacentes(){
		return posicionEnAdyacentes;
	}
	
	public void setPosicionEnAdyacentes (Position <Arco<V,E>> p){
		posicionEnAdyacentes= p;
		
	}
	
	public void setRotulo(E e){
		rotulo = e;
	}
	
}
