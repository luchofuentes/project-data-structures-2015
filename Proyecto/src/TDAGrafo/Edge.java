package TDAGrafo;

import TDALista.Position;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public interface Edge <E> extends Position <E> {
	
}
