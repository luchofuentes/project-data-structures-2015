package TDAGrafo;

import TDALista.Position;
import TDALista.PositionList;
import TDALista.DoubleLinkedList;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class Vertice<V, E> implements Vertex<V>{
	
	private V rotulo;
	private PositionList<Arco<V,E>> adyacentes;
	private Position<Vertice<V,E>> posicionEnNodos;
	
	public Vertice(V rotulo)
	{
		this.rotulo = rotulo;
		adyacentes = new DoubleLinkedList<Arco<V,E>>();
	}
	
	public V element()
	{
		return rotulo;
	}
	
	public void setRotulo(V nuevoRotulo)
	{
		rotulo= nuevoRotulo;
	}
	
	public PositionList<Arco<V,E>> getAdyacentes()
	{
		return adyacentes;
	
	}
	
	public void setPositionEnNodos(Position<Vertice<V,E>> P){
		posicionEnNodos = P;
	}
	
	public Position<Vertice<V,E>> getPositionEnNodos()
	{
		return posicionEnNodos;
	}

}
