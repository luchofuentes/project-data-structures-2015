package TDAGrafo;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class InvalidEdgeException extends Exception {

	/**
	 * Excepción que se lanza cuando un arco es invalido.
	 * @param msg Mensaje de error.
	 */
	public InvalidEdgeException (String msg){	
		super (msg);
	}
}
