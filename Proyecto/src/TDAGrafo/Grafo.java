package TDAGrafo;

import TDALista.*;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class Grafo <V,E> implements Graph<V,E>
{
	
	protected PositionList<Vertice<V,E>> nodos;

	public Grafo()
	{
		nodos = new DoubleLinkedList<Vertice<V,E>>();
	}

	public Iterable<Edge<E>> edges()
	{

		PositionList<Edge<E>> lista = new DoubleLinkedList<Edge<E>>();
	
		for(Vertice<V,E> v: nodos)
			for(Edge<E> e: successorEdges(v))
				lista.addLast(e);
		
		return lista;
	}

	public Iterable<Edge<E>> successorEdges(Vertex<V> v)
	{
		Vertice<V,E> vv = (Vertice<V,E>) v;
		PositionList<Edge<E>> lista = new DoubleLinkedList<Edge<E>>();
		
		for(Arco<V,E> e : vv.getAdyacentes())
			lista.addLast(e);
		
		return lista;
	}

	public Iterable<Vertex<V>> vertices() {
		
		PositionList<Vertex<V>> lista = new DoubleLinkedList<Vertex<V>>();
		
		for (Vertex<V> v: nodos)
			lista.addLast(v);
		
		return lista;
	}

	public Iterable<Edge<E>> incidentEdges(Vertex<V> v) throws InvalidVertexException {
		
		Vertice<V,E> vv = checkVertex(v);
		
		PositionList<Edge<E>> lista= new DoubleLinkedList<Edge<E>>();
		
		for(Edge<E> e : edges())
		{
			Arco<V,E> arc = (Arco<V,E>)e;
			if(arc.getSucesor().equals(vv))
				lista.addLast(e);
		}
		
		return lista;
	}
	
	public Iterable<Edge<E>> emergentEdges(Vertex<V> v) throws InvalidVertexException {
		
		Vertice<V,E> vv = checkVertex(v);
		
		PositionList<Edge<E>> lista= new DoubleLinkedList<Edge<E>>();
		
		for(Arco<V,E> ar: vv.getAdyacentes())
			lista.addLast(ar);
		return lista;
		
	}

	public Vertex<V> opposite(Vertex<V> v, Edge<E> e) throws InvalidVertexException, InvalidEdgeException {
		
		Vertice <V,E> vv= checkVertex(v);
		Arco <V,E> ee= checkEdge (e);
		
		if(ee.getPredecesor().equals(vv))
			return ee.getSucesor();
		else{
				throw new InvalidEdgeException("opposite: no tiene relacion el vertice v con el arco e");
			}
		}
		

	public Vertex<V>[] endVertices(Edge<E> e) throws InvalidEdgeException {
		
		Arco <V,E> ee= checkEdge(e);
		Vertex <V> [] arr= (Vertex <V> []) new Vertex [2];
		
		arr	[0]= ee.getPredecesor();
		arr [1]= ee.getSucesor();
		
		return arr;
	}

	public boolean areAdjacent(Vertex<V> v, Vertex<V> w) throws InvalidVertexException {
		
		Vertice <V,E> vv= checkVertex(v);
		Vertice <V,E>ww= checkVertex (w);
		
		for (Arco <V,E> arco: vv.getAdyacentes())
		{
			if(arco.getSucesor().equals(ww))
				return true;
			}
		return false;
	}

	public V replace(Vertex<V> v, V x) throws InvalidVertexException {
		
		Vertice<V,E> vv = checkVertex(v);
		V ret = vv.element();
		vv.setRotulo(x);
		
		return ret;
	}

	public E replace(Edge<E> e, E x) throws InvalidEdgeException {
		
		Arco<V,E> ee = checkEdge(e);
		E ret = ee.element();
		ee.setRotulo(x);
		
		return ret;
	}

	public Vertex<V> insertVertex(V x) {
		
		Vertice<V,E> v = new Vertice<V,E>(x);
		nodos.addLast(v);
		try{
			
			v.setPositionEnNodos(nodos.last());
		}catch (EmptyListException e){
			e.printStackTrace();
		}
		return v;
	}

	
	public Edge<E> insertDirectedEdge(Vertex<V> v, Vertex<V> w, E x) throws InvalidVertexException {
		
		Vertice<V,E> vv = checkVertex(v);
		Vertice<V,E> ww = checkVertex(w);
		
		Arco<V,E> arco = new Arco<V,E>(x,vv,ww);
		vv.getAdyacentes().addLast(arco);
		try {
			arco.setPosicionEnAdyacentes(vv.getAdyacentes().last());
		} 
		catch (EmptyListException e) {
			e.printStackTrace();
		}
		
		return arco;
	}

	public V removeVertex(Vertex<V> v) throws InvalidVertexException {
		
		Vertice<V,E> vv = checkVertex(v);
		
		try{
		for(Edge <E> e: incidentEdges(v))
			removeEdge(e);
		
		
		for (Edge <E> e: emergentEdges(v))
			removeEdge (e);
		
		nodos.remove(vv.getPositionEnNodos());
		
		}catch (InvalidPositionException e){
			e.printStackTrace();
		}
		catch (InvalidEdgeException e){
			e.printStackTrace();
		}
		
		return vv.element();
		
		
	}

	public E removeEdge(Edge<E> e) throws InvalidEdgeException {
		
		Arco<V,E> ee = checkEdge(e);
		Position<Arco<V,E>> pee = ee.getPosicionEnAdyacentes();
		Vertice<V,E> pred = ee.getPredecesor();
 		try {
			return pred.getAdyacentes().remove(pee).element();
		} 
 		catch (InvalidPositionException e1) {
			return null;
		}
 		
 		
	}
	
	/*##### Metodos Privados #####*/
	
	private Arco<V,E> checkEdge(Edge<E> e) throws InvalidEdgeException{
		
		if(e==null)
			throw new InvalidEdgeException("Arco invalido");
		
		try{
			return (Arco<V,E>) e;
		}
		
		catch (ClassCastException ee){
			throw new InvalidEdgeException("Arco invalido"); 
			
		}
	}
	
	private Vertice<V,E> checkVertex(Vertex<V> v) throws InvalidVertexException{
		
		if(v==null)
			throw new InvalidVertexException("Arco invalido");
		
		try{
			return (Vertice<V,E>) v;
		}
		
		catch (ClassCastException ee){
			throw new InvalidVertexException("Arco invalido"); 
			
		}
	}
	
}
	

		
