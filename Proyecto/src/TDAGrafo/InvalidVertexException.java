package TDAGrafo;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class InvalidVertexException extends Exception {
		
	/**
	 * Excepción que se lanza cuando el vertice es invalido.
	 * @param msg Mensaje de error.
	 */
	public InvalidVertexException (String msg){
		super (msg);
	}
}
