package TDALista;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class BoundaryViolationException extends Exception{
	
	/**
	 * Excepción que se lanza al pedir una posicion al elemento equivocado.
	 * @param msg Mensaje de error.
	 */
	public BoundaryViolationException(String msg){
		super(msg);
	}
}
