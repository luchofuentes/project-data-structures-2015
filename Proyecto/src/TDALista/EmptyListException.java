package TDALista;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class EmptyListException extends Exception {
	
	/**
	 * Excepción que se lanza cuando se pide un elemento a una lista vacía.
	 * @param msg Mensaje de error.
	 */
	public EmptyListException (String msg){
		super(msg);
	}
}
	
