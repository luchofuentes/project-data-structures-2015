package TDALista;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public interface Position<E> {
	
	/**
	 * Retorna el elemento de la posición.
	 * @return un elemento de la posición.
	 */	
	public E element();
}
