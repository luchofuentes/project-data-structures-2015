package TDALista;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class InvalidPositionException extends Exception{
	
	/**
	 * Excepcon que se lanza cuando una posicion es invalida.
	 * @param msg Mensaje de error.
	 */
	public InvalidPositionException (String msg){
		super(msg);
	}
}