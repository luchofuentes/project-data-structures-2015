package TDALista;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class DNodo <E> implements Position <E>{
	private DNodo<E> anterior, siguiente; 
	private E element;
	
	public DNodo(DNodo<E> ant, DNodo<E> sig, E elem){
		anterior = ant;
		siguiente = sig;
		element= elem;
	}
	
	public E element(){
		
		return element;
	}
	/**
	 * Retorna el nodo siguiente al que le pase el mensaje.
	 * @return un nodo siguiente.
	 */	
	public DNodo<E> getSiguiente(){
		return siguiente;
	}
	/**
	 * Retorna el nodo anterior al que le pase el mensaje.
	 * @return un nodo anterior.
	 */	
	public DNodo<E> getAnterior(){
		return anterior;
	}
	/**
	 * Inserta el elemento.
	 * @param elem elemento a insertar.
	 */	
	public void setElement(E elem){
		element = elem;
	}
	/**
	 * Inserta el elemento siguiente.
	 * @param sig nodo a insertar.
	 */	
	public void setSiguiente(DNodo<E> sig){
		siguiente=sig;
	}
	/**
	 * Inserta el elemento anterior.
	 * @param ant nodo a insertar.
	 */	
	public void setAnterior(DNodo<E> ant){
		anterior=ant;
	}
	
	
}

