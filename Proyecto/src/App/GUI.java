package App;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import TDAGrafo.*;
import TDALista.PositionList;


/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class GUI extends JFrame {
	
	
	private Logica log;
	private JMenuBar menuBar;
	private JMenu mnNuevo;
	private JMenuItem mntmGrafo;
	private JButton btnAgregarVertice;
	private JButton btnAgregarArco;
	private JButton btnEliminarVertice;
	private JButton btnRecorridoEnProfundidad;
	private JButton btnRecorridoEnAnchura;
	private JButton btnAplicacion1;
	private JButton btnAplicacion2;
	private JButton btnAplicacion3;
	private JPanel contentPane;
	private JScrollPane pane;
	private JTextArea area;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Inicializa los componentes de la interfaz.
	 */
	public GUI() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 678, 453);
		setTitle("Proyecto");
		
				
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnNuevo = new JMenu("Nuevo");
		menuBar.add(mnNuevo);
		
		mntmGrafo = new JMenuItem("Grafo");
		mntmGrafo.addActionListener(new OyenteNuevoGrafo());
		mnNuevo.add(mntmGrafo);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		btnAgregarVertice = new JButton("Agregar Vertice");
		btnAgregarVertice.setEnabled(false);
		btnAgregarVertice.addActionListener(new OyenteAgregarVertice());
		
		btnAgregarArco = new JButton("Agregar Arco");
		btnAgregarArco.addActionListener(new OyenteAgregarArco());
		btnAgregarArco.setEnabled(false);
		
		btnEliminarVertice = new JButton("Eliminar Vertice");
		btnEliminarVertice.addActionListener(new OyenteEliminarVertice());
		btnEliminarVertice.setEnabled(false);
		
		btnRecorridoEnProfundidad = new JButton("Recorrido en profundidad");
		btnRecorridoEnProfundidad.addActionListener(new OyenteRecorridoEnProfundidad());
		btnRecorridoEnProfundidad.setEnabled(false);
		
		btnRecorridoEnAnchura = new JButton("Recorrido en anchura");
		btnRecorridoEnAnchura.addActionListener(new OyenteRecorridoEnAnchura());
		btnRecorridoEnAnchura.setEnabled(false);
		
		btnAplicacion1 = new JButton("Camino mas corto");
		btnAplicacion1.addActionListener(new OyenteAplicacion1());
		btnAplicacion1.setEnabled(false);
		
		btnAplicacion2 = new JButton("Todos los caminos");
		btnAplicacion2.addActionListener(new OyenteAplicacion2());
		btnAplicacion2.setEnabled(false);
		
		
		btnAplicacion3 = new JButton("Eliminar camino mas corto");
		btnAplicacion3.addActionListener(new OyenteAplicacion3());
		btnAplicacion3.setEnabled(false);
		
		contentPane.setLayout(new GridLayout(1,2,5,5));
		
		pane = new JScrollPane();
        area = new JTextArea();

        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setEditable(false);
        area.setBackground(new Color(224,224,224));
        
        pane.getViewport().add(area);

		JPanel panelbuttons = new JPanel(new GridLayout(8,1,5,5));
		
		panelbuttons.setBackground(new Color(224,224,224));
		panelbuttons.add(btnAgregarVertice);
		panelbuttons.add(btnAgregarArco);
		panelbuttons.add(btnEliminarVertice);
		panelbuttons.add(btnRecorridoEnProfundidad);
		panelbuttons.add(btnRecorridoEnAnchura);
		panelbuttons.add(btnAplicacion1);
		panelbuttons.add(btnAplicacion2);
		panelbuttons.add(btnAplicacion3);

		
		contentPane.setBackground(new Color(224,224,224));
		contentPane.add(panelbuttons);
		contentPane.add(pane);
	}
	
	private String imprimirVertices(){
		String s = "Vertices: ";
		for(Vertex<Integer> v: log.listaDeVertices()){
			s+=v.element();
			s+=", ";
		}
		
		return s;
	}
	
	private String imprimirArcos(){
		String s = "Arcos: ";
		
		for(Vertex<Integer>[] e: log.extremosDeArcos()){
			s+="("+e[0].element()+" ------>"+e[1].element()+"), ";
		}
		
		return s;
	}
	
	private void imprimir(){
		String s="";
		s+=imprimirVertices();
		s+="\n";
		s+=imprimirArcos();
		s+="\n";
		area.setText(s);
	}
	
	private class OyenteNuevoGrafo implements ActionListener{
		
        public void actionPerformed(ActionEvent e){
	       	log = new Logica();
	        log.cargarGrafo(); 	
	        imprimir();
	        btnAgregarVertice.setEnabled(true);
	        btnAgregarArco.setEnabled(true);
	        btnEliminarVertice.setEnabled(true);
	        btnRecorridoEnProfundidad.setEnabled(true);
	        btnRecorridoEnAnchura.setEnabled(true);
	        btnAplicacion1.setEnabled(true);
	        btnAplicacion2.setEnabled(true);
	        btnAplicacion3.setEnabled(true);
        }
	}
    
	private class OyenteAgregarVertice implements ActionListener{
       
		public void actionPerformed(ActionEvent e){
            
			JTextField vertice = new JTextField();
			JLabel label = new JLabel("Ingrese el rotulo del vertice:");
			final JComponent[] inputs = new JComponent[] { label, vertice };
			
			int result = JOptionPane.showConfirmDialog(contentPane, inputs, "Agregar Vertice", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if(result==JOptionPane.OK_OPTION){
					try{
						log.agregarVertice(Integer.parseInt(vertice.getText()));
						imprimir();
					}
					catch(NumberFormatException exp){
						JOptionPane.showMessageDialog(contentPane, "Solo puede ingresar numeros enteros","Agregar Vertices",JOptionPane.ERROR_MESSAGE);
					}
					
			}
		}
	}
    
	
	private class OyenteAgregarArco implements ActionListener{
        
		public void actionPerformed(ActionEvent e){
			
			JTextField origen  = new JTextField();
			JTextField destino = new JTextField();
			final JComponent[] inputs = new JComponent[] {
					new JLabel("Ingrese el origen:"),
					origen,
					new JLabel("Ingrese el destino: "),
					destino
			};
			int result = JOptionPane.showConfirmDialog(contentPane, inputs, "Agregar Arco", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if (result==JOptionPane.OK_OPTION){
				try{
					log.agregarArco(Integer.parseInt(origen.getText()),Integer.parseInt(destino.getText()));
					imprimir();
				}
				catch(InvalidEdgeException exp){
					JOptionPane.showMessageDialog(contentPane, exp.getMessage(),"Agregar Arcos",JOptionPane.ERROR_MESSAGE);
				}
				catch(InvalidVertexException exp){
					JOptionPane.showMessageDialog(contentPane, exp.getMessage(),"Agregar Arcos",JOptionPane.ERROR_MESSAGE);
				}
				catch(NumberFormatException exp){
					JOptionPane.showMessageDialog(contentPane, "Solo puede ingresar numeros enteros","Agregar Arcos",JOptionPane.ERROR_MESSAGE);
				}
			}
        }
    }
	
	private class OyenteEliminarVertice implements ActionListener{
       
		public void actionPerformed(ActionEvent e){
			
			JTextField vertice  = new JTextField();
			final JComponent[] inputs = new JComponent[] {
					new JLabel("Ingrese el vertice:"),
					vertice,
			};
			int result = JOptionPane.showConfirmDialog(contentPane, inputs, "Eliminar Vertice", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if(result==JOptionPane.OK_OPTION){
				try {
					
					log.eliminarVertice(Integer.parseInt(vertice.getText()));
					imprimir();
				} 
				catch (InvalidVertexException exp) {
					JOptionPane.showMessageDialog(contentPane, exp.getMessage(),"Eliminar Vertice",JOptionPane.ERROR_MESSAGE);
				}
				catch(NumberFormatException exp){
					JOptionPane.showMessageDialog(contentPane, "Solo puede ingresar numeros enteros","Eliminar Vertice",JOptionPane.ERROR_MESSAGE);
				}
			}
        }
    }
	
	private class OyenteRecorridoEnProfundidad implements ActionListener{
 
		public void actionPerformed(ActionEvent e){
			
			imprimir();
			String s = area.getText();
			s+="Recorrido en profundidad: ";
			for(Vertex<Integer> v : log.recorridoEnProfundidad()){
				s+=v.element();
				s+=", ";
			}
			area.setText(s);
            
        }
    }


	private class OyenteRecorridoEnAnchura implements ActionListener{
    
		public void actionPerformed(ActionEvent e){
			
			imprimir();
			String s = area.getText();
			s+="Recorrido en Anchura: ";
			for(Vertex<Integer> v : log.recorridoEnAnchura()){
				s+=v.element();
				s+=", ";
			}
			area.setText(s);
        
		}
	}

	private class OyenteAplicacion1 implements ActionListener{

		public void actionPerformed(ActionEvent e){
			
			JTextField origen  = new JTextField();
			JTextField destino = new JTextField();
			final JComponent[] inputs = new JComponent[] {
					new JLabel("Ingrese el origen:"),
					origen,
					new JLabel("Ingrese el destino: "),
					destino
			};
			int result = JOptionPane.showConfirmDialog(contentPane, inputs, "Camino mas Corto", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if (result==JOptionPane.OK_OPTION){
				try{
					
					Iterable<Vertex<Integer>> p = log.caminoMinimo(Integer.parseInt(origen.getText()),Integer.parseInt(destino.getText()));
					imprimir();
					String s = area.getText();
					int cont=0;
					s+="Camino mas corto: ";
					for(Vertex<Integer> v : p){
						s+=v.element();
						s+=", ";
						cont++;
					}
					if(cont==0)
						s+="No hay camino entre los dos vertices.";
					area.setText(s);
					
				}
				catch(InvalidVertexException exp){
					JOptionPane.showMessageDialog(contentPane, exp.getMessage(),"Camino mas corto",JOptionPane.ERROR_MESSAGE);
				}
				catch(NumberFormatException exp){
					JOptionPane.showMessageDialog(contentPane, "Solo puede ingresar numeros enteros","Camino mas corto",JOptionPane.ERROR_MESSAGE);
				}
			}
        
		}
	}

	private class OyenteAplicacion2 implements ActionListener{
    
		public void actionPerformed(ActionEvent e){
			
			JTextField origen  = new JTextField();
			JTextField destino = new JTextField();
			final JComponent[] inputs = new JComponent[] {
					new JLabel("Ingrese el origen:"),
					origen,
					new JLabel("Ingrese el destino: "),
					destino
			};
			int result = JOptionPane.showConfirmDialog(contentPane, inputs, "Todos los caminos", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if (result==JOptionPane.OK_OPTION){
				try{
					
					Iterable<PositionList<Vertex<Integer>>> p = log.TodosLosCaminos(Integer.parseInt(origen.getText()),Integer.parseInt(destino.getText()));
					imprimir();
					String s = area.getText();
					s+="Todos los caminos: \n";
					int cont=0;
					int i=1;
					for(PositionList<Vertex<Integer>> pv : p){
						cont++;
						s+=i+"° : ";
						for(Vertex<Integer> v : pv){
							s+=v.element();
							s+=", ";
						}
						s+="\n";
						i++;
					}
					if(cont==0)
						s+="No hay camino entre los dos vertices.";
					area.setText(s);
					
				}
				catch(InvalidVertexException exp){
					JOptionPane.showMessageDialog(contentPane, exp.getMessage(),"Todos los caminos",JOptionPane.ERROR_MESSAGE);
				}
				catch(NumberFormatException exp){
					JOptionPane.showMessageDialog(contentPane, "Solo puede ingresar numeros enteros","Tods los caminos",JOptionPane.ERROR_MESSAGE);
				}
			}
        
		}
	}

	private class OyenteAplicacion3 implements ActionListener{
    
		public void actionPerformed(ActionEvent e){
			
			
			JTextField origen  = new JTextField();
			JTextField destino = new JTextField();
			final JComponent[] inputs = new JComponent[] {
					new JLabel("Ingrese el origen:"),
					origen,
					new JLabel("Ingrese el destino: "),
					destino
			};
			int result = JOptionPane.showConfirmDialog(contentPane, inputs, "Eliminar camino mas corto", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
			if (result==JOptionPane.OK_OPTION){
				try{
					int eliminados = log.eliminarCaminoMinimo(Integer.parseInt(origen.getText()),Integer.parseInt(destino.getText()));
					if(eliminados==0)
						JOptionPane.showMessageDialog(contentPane, "No se han eliminado elementos","Eliminar camino mas corto",JOptionPane.ERROR_MESSAGE);
					else
						JOptionPane.showMessageDialog(contentPane, "Se han eliminado "+eliminados+" elementos","Eliminar camino mas corto",JOptionPane.INFORMATION_MESSAGE);
					imprimir();
				}
				catch(InvalidVertexException exp){
					JOptionPane.showMessageDialog(contentPane, exp.getMessage(),"Eliminar camino mas corto",JOptionPane.ERROR_MESSAGE);
				}
				catch(NumberFormatException exp){
					JOptionPane.showMessageDialog(contentPane, "Solo puede ingresar numeros enteros","Eliminar camino mas corto",JOptionPane.ERROR_MESSAGE);
				}
			
        
			}
		}
	}

}