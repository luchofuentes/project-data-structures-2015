package App;
import java.util.Iterator;

import TDAGrafo.*;
import TDAMapeo.*;
import TDALista.*;

/**
 * @author Luciano Fuentes, Salvador Catalfamo.
 */

public class Logica {
	
	
	//Atributos
	Graph<Integer, Integer> g;
	
	/**
	 * Crear un nuevo grafo vacio
	 */
	public void cargarGrafo (){
		
		g= new Grafo <Integer,Integer>();
	
	}
	
	/**
	 * Crea un nuevo vertice con rotulo n
	 * @param n rotulo del nuevo vertice
	 * @return rotulo del nuevo vertice
	 */
	public int agregarVertice (int n){
		
		Vertex <Integer> toRet= g.insertVertex(n);
		
		return (toRet.element());
		
	}
	
	/**
	 * Agrega un nuevo arco con origen o y destino d, si el arco ya existe no lo inserta.
	 * @param o origen del arco
	 * @param d destino del grafo
	 * @throws InvalidVertexException si alguno de los dos vertices son inválidos
	 */
	public void agregarArco (int o, int d) throws InvalidVertexException, InvalidEdgeException{
		
		Vertex <Integer> origen  = buscarVertice (o);
		Vertex <Integer> destino = buscarVertice (d);
		
		boolean agregar = true;
		
		for(Vertex<Integer>[] v : extremosDeArcos()){
			if(v[0].element()==o && v[1].element()==d){
				agregar=false;
				throw new InvalidEdgeException("El arco ya existe.");
			}
		}
		
		if(agregar)
			g.insertDirectedEdge(origen, destino , null);
	
	}
	
	/**
	 * Elimina el vertice con rotulo n
	 * @param n rotulo del vertice a eliminar
	 * @return el rotulo del vertice eliminado
	 * @throws InvalidVertexException si el vertice no es valido
	 */
	public int eliminarVertice (int n) throws InvalidVertexException {
		
		Vertex<Integer> vAElim= buscarVertice(n);
		
		return g.removeVertex(vAElim);
		
	}
	
	/**
	 * Realiza un recorrido en profundidad
	 * @return una lista con los elementos del recorrido
	 */
	public Iterable<Vertex<Integer>> recorridoEnProfundidad (){
	
		PositionList <Vertex<Integer>> lista = new DoubleLinkedList <Vertex<Integer>>();
		Map <Vertex<Integer>,Boolean>      m = new MapeoConHash <Vertex<Integer>,Boolean>();
		
		for(Vertex <Integer> v: g.vertices())
			try {
				m.put(v, false);
			} catch (InvalidKeyException e) {
				
				e.printStackTrace();
			}
		
		for (Vertex <Integer> v: g.vertices())
			try {
				if(!m.get(v))
					recorridoEnProfundidadAux(v,lista,m);
			} catch (InvalidKeyException e) {
			
				e.printStackTrace();
			}
		
		return lista;
	}
	
	/**
	 * Auxiliar de recorrido en profundidad
	 * @param v vertice actual
	 * @param l lista donde esta el camino
	 * @param m mapeo de marca
	 */
	private void recorridoEnProfundidadAux(Vertex <Integer> v, PositionList<Vertex<Integer>> l,Map <Vertex<Integer>,Boolean> m){
		
		try{
			l.addLast(v);
			m.put(v, true);
			for(Edge <Integer> e: g.emergentEdges(v))
				if (!m.get(g.opposite(v, e)))
					recorridoEnProfundidadAux(g.opposite(v, e),l,m);
			
		}	
		catch(InvalidVertexException | InvalidEdgeException | InvalidKeyException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Realiza un recorrido en Anchura
	 * @return Una lista con los elementos de la busqueda
	 */
	public Iterable<Vertex<Integer>> recorridoEnAnchura (){
		
		PositionList<Vertex<Integer>> lista = new DoubleLinkedList <Vertex<Integer>>();
		Map <Vertex<Integer>,Boolean>     m = new MapeoConHash <Vertex<Integer>,Boolean>();
		
		for(Vertex <Integer> v: g.vertices())
			try {
				m.put(v, false);
			} catch (InvalidKeyException e) {
				
				e.printStackTrace();
			}
		
		for (Vertex <Integer> v: g.vertices())
			try {
				if(!m.get(v))
					busquedaEnAnchuraAux(v,lista,m);
			} catch (InvalidKeyException e) {
			
				e.printStackTrace();
			}
		
		return lista;
	}
	
	private void busquedaEnAnchuraAux (Vertex <Integer> v,PositionList<Vertex<Integer>> l, Map <Vertex<Integer>,Boolean> m){
		
		PositionList<Vertex<Integer>> lista = new DoubleLinkedList <Vertex<Integer>>();
		lista.addLast(v);
		
		while(!lista.isEmpty()){
			try {
				Vertex<Integer> vert= lista.remove(lista.first());
				l.addLast(vert);
				m.put(vert, true);
				for(Edge <Integer> e :g.emergentEdges(vert)){
					Vertex<Integer> aVisitar= g.opposite(vert, e);
					if(!m.get(aVisitar)){
						m.put(aVisitar, true);
						lista.addLast(aVisitar);
					}
				}
			} catch (InvalidPositionException | EmptyListException | InvalidKeyException | InvalidVertexException |InvalidEdgeException e) {
				e.printStackTrace();
			}
		}
		
		
	}
	
	/**
	 * Aplicacion 1: halla el camino minimo entre dos vertices
	 * @param o origen
	 * @param d destino
	 * @return Una lista con los vertices del camino mas corto 
	 * @throws InvalidVertexException si el origen o el destino no es valido
	 */
	public Iterable<Vertex<Integer>> caminoMinimo(int o, int d) throws InvalidVertexException{
		 
		 Vertex<Integer> origen  = buscarVertice (o); 
		 Vertex<Integer> destino = buscarVertice (d);
	 
		 Map<Vertex<Integer>,Boolean> map = new MapeoConHash<Vertex<Integer>,Boolean>();
    
		 for(Vertex<Integer> v : g.vertices())
             try{
            	 map.put(v, false);
             } 
		 	 catch (InvalidKeyException e){    
                     e.printStackTrace();
             }
    
     	PositionList<PositionList<Vertex<Integer>>> S = new DoubleLinkedList<PositionList<Vertex<Integer>>>(); //Lista de caminos.
     	PositionList<Vertex<Integer>>          camino = new DoubleLinkedList<Vertex<Integer>>(); //Camino actual            
     	HallarCamino(origen,destino,camino,S,map);
                                    
        return caminoMasCorto(S);
	}
	 
	/**
	 * Metodo auxiliar para hallar el camino minimo
	 * @param origen 
	 * @param destino
	 * @param Camino actual
	 * @param S lista con todos los caminos
	 * @param map mapeo de marca de vertices
	 */
	private void HallarCamino(Vertex<Integer> origen, Vertex<Integer> destino, PositionList<Vertex<Integer>> camino, PositionList<PositionList<Vertex<Integer>>> S, Map<Vertex<Integer>,Boolean> map){
    
		 try{
             camino.addLast(origen);
             map.put(origen, true);
             
             if(origen==destino){
                    
                     PositionList<Vertex<Integer>> nuevalista = new DoubleLinkedList<Vertex<Integer>>();
                     for(Vertex<Integer> v : camino){
                             nuevalista.addLast(v);
                     }
                     S.addLast(nuevalista);
                     map.put(origen, false);
                     camino.remove(camino.last());
             }
             else{
                    
                     for(Edge<Integer> e : g.emergentEdges(origen))
                                     if(!map.get(g.opposite(origen,e))){
                                             HallarCamino(g.opposite(origen,e),destino,camino,S,map);
                                            
                                    
                     }
                     map.put(origen, false);
                     camino.remove(camino.last());
             }
            
            
            
		 }
		 catch(InvalidKeyException | InvalidEdgeException |  InvalidVertexException | InvalidPositionException | EmptyListException exp){
             	exp.printStackTrace();
         }
	 }

	/**
	 * Aplicacion 2: Dados 2 vertices, halla todos los caminos entre ellos 
	 * @param o origen
	 * @param d destino
	 * @return Una lista con listas de todos los caminos
	 * @throws InvalidVertexException si el origen o el destino son invalidos
	 */
	public Iterable<PositionList<Vertex<Integer>>> TodosLosCaminos(int o, int d) throws InvalidVertexException{
		
		Vertex<Integer> origen= buscarVertice (o);
		Vertex<Integer> destino= buscarVertice (d);
	                       
			Map<Vertex<Integer>,Boolean> map = new MapeoConHash<Vertex<Integer>,Boolean>();
	                    
	        for(Vertex<Integer> v : g.vertices())
	        try{
				map.put(v, false);
	        } 
	        catch (InvalidKeyException e) {
				e.printStackTrace();
	        }
	                       
			PositionList<PositionList<Vertex<Integer>>> S = new DoubleLinkedList<PositionList<Vertex<Integer>>>();                                     
	        PositionList<Vertex<Integer>> camino = new DoubleLinkedList<Vertex<Integer>>();            
	        HallarCaminoTodos(origen,destino,camino,S,map);
	                                                       
			return S;
	}
	               
	private void HallarCaminoTodos(Vertex<Integer> origen, Vertex<Integer> destino, PositionList<Vertex<Integer>> camino, PositionList<PositionList<Vertex<Integer>>> S, Map<Vertex<Integer>,Boolean> map){
	                       
		try{
			camino.addLast(origen);
	        map.put(origen, true);
	 
	        if(origen==destino){
	                                       
	        	PositionList<Vertex<Integer>> nuevalista = new DoubleLinkedList<Vertex<Integer>>();
	            	for(Vertex<Integer> v : camino)
	            		nuevalista.addLast(v);
	            S.addLast(nuevalista);
	        }	
	        else{
	                                       
				for(Edge<Integer> e : g.emergentEdges(origen))
					if(!map.get(g.opposite(origen,e)))
						HallarCaminoTodos(g.opposite(origen,e),destino,camino,S,map);

			}
				map.put(origen, false);
	            camino.remove(camino.last());                       
			}
	        catch(InvalidKeyException | InvalidEdgeException |  InvalidVertexException | InvalidPositionException | EmptyListException exp){
				exp.printStackTrace();
	        }
	}
	
	/**
	 * Dada una lista de caminos, retorna la lista que tenga el camino mas corto 
	 * @param S Lista de lista de caminos 
	 * @return Una lista con el camino mas corto
	 */
	private Iterable<Vertex<Integer>> caminoMasCorto(PositionList<PositionList<Vertex<Integer>>> S){
        
		PositionList<Vertex<Integer>> corto = new DoubleLinkedList<Vertex<Integer>>();
		
		if(S.size() > 0)
		try{
			corto = S.first().element();
		} catch (EmptyListException e) {
			e.printStackTrace();
		}
        
        for(PositionList<Vertex<Integer>> p : S)
        	if(p.size() < corto.size())
        		corto = p;
        
         return corto;
	}
	
	/**
	 * Aplicacion 3: dados dos vertices, elimina el camino minimo entre ellos
	 * @param o origen
	 * @param d destino
	 * @throws InvalidVertexException si alguno de los dos vertices son invalidos
	 */
	public int eliminarCaminoMinimo (int o, int d) throws InvalidVertexException{
			
			int ret=0;
			for (Vertex <Integer> v: caminoMinimo(o,d))
				if(v.element()!=o && v.element()!=d){
					ret++;
					g.removeVertex(v);
				}
			
			return ret;
	}
	
	/**
	 * Dado un entero, encuentra su vertice asociado en el grafo
	 * @param n Rotulo del grafo a buscar
	 * @return vertice asociado en el grafo
	 * @throws InvalidVertexException si el vertice no se encuentra en el grafo
	 */
	private Vertex<Integer> buscarVertice(int n) throws InvalidVertexException{
			
		Iterator<Vertex<Integer>> it= g.vertices().iterator();
		boolean encontre= false;
			
		while (it.hasNext() && !encontre){
			Vertex <Integer> pos=it.next();
				
			if (pos.element()==n)
				return pos;
			}
					
			if (!encontre)
				throw new InvalidVertexException ("El vertice no se encuentra en el grafo");
			
			return null;
	}
	
	/**
	 * Retorna una coleccion iterable con todos los vertices del grafo
	 * @return Coelccion iteravle de vertices
	 */
	public Iterable<Vertex<Integer>> listaDeVertices(){

		PositionList<Vertex<Integer>> lista= new DoubleLinkedList <Vertex<Integer>> ();
		
	    for(Vertex <Integer> e : g.vertices())
	    	lista.addLast(e);
	    	
	    return lista;	
	}
	
	/**
	 * Retorna un mapeo representando los extremos de los arcos, donde el key es el origen, y el value es el destino
	 * @return Un mapeo representando los extremos de los arcos
	 */
	public Iterable<Vertex<Integer>[]> extremosDeArcos (){
		
		PositionList<Vertex<Integer>[]> lista = new DoubleLinkedList<Vertex<Integer>[]>();
		
		for (Edge<Integer> e: g.edges()){
			try{
				lista.addLast(g.endVertices(e));
			}catch (InvalidEdgeException ee){
				ee.printStackTrace();
			}
		}
		
		return lista;	
			
	}
	
}